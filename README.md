This is a little collection of scripts to apply voltages and clocks to AMD GPUs on Linux.

## Why use this instead of CoreCtrl/GUI tools
CoreCtrl hogs about 200MB of RAM on my system just to sit in the system tray and apply some clocks on boot.

## Installation
    git clone https://gitlab.com/Renner0E/amd-gpu-oc
    cd amd-gpu-oc
    sudo cp overclock /usr/local/bin && sudo chmod +x /usr/local/bin/overclock
    sudo cp overclock.service /etc/system/systemd
    sudo systemctl enable overclock.service

## Setup
Follow Instructions at https://wiki.archlinux.org/title/AMDGPU#Overclocking

## Set Kernel Boot Parameters
Append amdgpu.ppfeaturemask=0xffffffff to either:

/etc/default/grub if you use grub

If you use systemd-boot edit $(bootctl status | grep source | awk 'NF>1{print $NF}')

Make sure to make a copy of the file before you make any changes in case somethings fails.
